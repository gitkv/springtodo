package com.example.demo.entities;

import com.example.demo.views.TodoView;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
public class Todo extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonView(TodoView.IdTextDone.class)
    private Long id;

    @JsonView(TodoView.IdTextDone.class)
    private String text;

    @JsonView(TodoView.IdTextDone.class)
    @Column(columnDefinition = "boolean default false")
    private Boolean done;
}
