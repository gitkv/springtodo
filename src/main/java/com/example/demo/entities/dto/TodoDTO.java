package com.example.demo.entities.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class TodoDTO {

    @NotEmpty(message = "Поле \"текст\" должно быть заполнено")
    private String text;

    private Boolean done;
}
