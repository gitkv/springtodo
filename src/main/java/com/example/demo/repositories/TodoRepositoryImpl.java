package com.example.demo.repositories;

import com.example.demo.entities.Todo;
import com.example.demo.exceptions.ModelNotFoundException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class TodoRepositoryImpl implements TodoRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public TodoRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Todo> list() {
        return entityManager.createQuery("from Todo t", Todo.class).getResultList();
    }

    @Override
    public Todo getTodoById(Long id) {
        Todo todo = entityManager.find(Todo.class, id);
        if (null == todo) {
            throw new ModelNotFoundException();
        }

        return todo;
    }

    @Override
    public Todo save(Todo todo) {
        if (todo.getId() == null) {
            entityManager.persist(todo);
        } else {
            todo = entityManager.merge(todo);
        }

        return todo;
    }

    @Override
    public Todo delete(Todo todo) {
        if (entityManager.contains(todo)) {
            entityManager.remove(todo);
        } else {
            entityManager.merge(todo);
        }

        return todo;
    }
}
