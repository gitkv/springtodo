package com.example.demo.repositories;

import com.example.demo.entities.Todo;

import java.util.List;

public interface TodoRepository {

    List<Todo> list();

    Todo getTodoById(Long id);

    Todo save(Todo todo);

    Todo delete(Todo todo);
}
