package com.example.demo.controllers;

import com.example.demo.entities.Todo;
import com.example.demo.entities.dto.TodoDTO;
import com.example.demo.repositories.TodoRepository;
import com.example.demo.views.TodoView;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/todo")
public class TodoController {

    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    public TodoController(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @RequestMapping
    @JsonView(TodoView.IdTextDone.class)
    public List<Todo> index() {
        return todoRepository.list();
    }

    @GetMapping("{id}")
    public Todo show(@PathVariable("id") Long id) {
        return todoRepository.getTodoById(id);
    }

    @PostMapping
    public Todo create(@Valid @RequestBody TodoDTO todoDTO, Todo todo) {
        BeanUtils.copyProperties(todoDTO, todo);

        return todoRepository.save(todo);
    }

    @PutMapping("{id}")
    public Todo update(@PathVariable("id") Long id, @RequestBody TodoDTO todoDTO) {
        Todo todo = todoRepository.getTodoById(id);
        BeanUtils.copyProperties(todoDTO, todo);

        return todoRepository.save(todo);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Long id) {
        Todo todo = todoRepository.getTodoById(id);
        todoRepository.delete(todo);
    }
}
