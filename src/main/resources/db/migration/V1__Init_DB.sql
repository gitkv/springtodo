create sequence hibernate_sequence start 1 increment 1;

create table todo
(
    id         int8         not null,
    text       varchar(255) not null,
    done       boolean default false,
    created_at timestamp    not null,
    updated_at timestamp    not null,
    primary key (id)
);